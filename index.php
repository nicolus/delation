<?php
include "db/model.php";
include "config.php";

if (isset($_POST['commentaire'])){
	addDelation($_POST['corbeau'], $_POST['coupable'], $_POST['commentaire']);
	
	$corbeau = getMemberName($_POST['corbeau']);
	$coupable = getMemberName($_POST['coupable']);
	$commentaire = $_POST['commentaire'];
	
	require_once('/twitteroauth/twitterOAuth.php');
    $tweet = new TwitterOAuth(TWITTER_CONSUMER_KEY, TWITTER_CONSUMER_SECRET, TWITTER_OAUTH_TOKEN, TWITTER_OAUTH_SECRET);
    $tweet->post('statuses/update', array('status' => "$coupable $commentaire"));
}

$membres = getMembers();
$infractions = getInfractions();
$classement = getDelationsCount();
?>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title></title>
		<link rel="stylesheet" href="style/main.css" type="text/css" />
    </head>
    <body>
		<h1><img src="img/header.jpg" /></h1>
		<form method=POST action=''>
			<label>Corbeau :</label>
			<select name=corbeau>
				<? foreach ($membres as $membre): ?>
				<option value="<?=$membre['id']?>"><?=$membre['name']?></option>
				<? endforeach ?>
			</select>
			<br>
			<label>Coupable :</label>
			<select name=coupable>
				<? foreach ($membres as $membre): ?>
				<option value="<?=$membre['id']?>"><?=$membre['name']?></option>
				<? endforeach ?>
			</select>
			<br>
			<!--<label>Infraction :</label>
			<select name=infraction>
				<? foreach ($infractions as $infraction): ?>
				<option value="<?=$infraction['id']?>"><?=$infraction['name']?></option>
				<? endforeach ?>
			</select> 
			<br>-->
			<label>Commentaire :</label>
			<input type=text name=commentaire />
			<br>
			<input type=submit />
		</form>
		
		<h2>Les meilleurs délateurs (aka : les chouchous) :</h2>
        <table>
            <tr>
                <td>Délations</td>
                <td>Employé</td>
            </tr>
            <?php foreach ($classement as $chouchou): ?>
            <tr>
                <td><?= $chouchou['nb_delation'] ?></td>
                <td><?= $chouchou['nom'] ?></td>
            </tr>
            <?php endforeach ?>
        </table>
    </body>
</html>
