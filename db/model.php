<?php
/**
 * Exécute une Requete SQL.
 * @param string $req La requête à exécuter.
 * @param array $params Les paramètres
 * @return object Le résultat de la requête
 */
function execute($req,$params=array()){
	try {
		$db = new PDO('sqlite:db/db.sqlite');
		$stmt = $db->prepare($req);
		$stmt->execute($params);
		$res = $stmt->fetchAll();
	} catch (PDOException $e) {
		print "Erreur !: " . $e->getMessage() . "<br>";
		die();
	}
    return $res;
}

/**
 * 
 * @return type
 */
function getMembers(){
	$sql = "SELECT id, name FROM member";
	return execute($sql);
}




function getInfractions(){
	$sql = "SELECT id, name FROM type";
	return execute($sql);
}


function getDelationsCount(){
	$sql = "SELECT name as nom, COUNT(*) as nb_delation
			FROM delation d
			INNER JOIN member m ON m.id = d.id_delator
			GROUP BY name
			ORDER BY nb_delation DESC";
	return execute($sql);
}


function addDelation($id_delator, $id_victime, $comment){
	$sql = "INSERT INTO delation (id,id_delator,id_victim,date,comment)
			VALUES (NULL,?,?,'NOW',?)";
	return execute($sql,array($id_delator, $id_victime, $comment));
}

function getMemberName($id){
	$sql = "SELECT name FROM member WHERE id=?";
	return execute($sql,array($id))[0][0];
}



?>
